import tweepy
import config

print("Bot is Running")


auth = tweepy.OAuth1UserHandler(
    config.consumer_key,
    config.consumer_secret,
    config.access_token,
    config.access_token_secret,
)

api = tweepy.API(auth)


def tweeting_function():
    print("Sending Tweet")
    message = "This is an automated tweet 🎃"
    api.update_status(status=message)


tweeting_function()
